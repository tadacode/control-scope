# อยากสร้าง App ให้ใช้ App sheet 
<br> แทน MIT appinventor App Sheet พัฒนาต่อได้มากกว่าอยู่ใน Google Scope
<br>[google sheet to app sheet](https://www.appsheet.com/)
<br>วิธีใช้งานโหลด Appsheet ใน google play ใช้ได้เลย เวลาเราสร้างต้องการแชร์ให้เพื่อนก็ทำได้ เพื่อนมี Appsheet ก็ใช้ได้เลย
<br>สร้างแอฟง่ายมากและแชร์ได้ง่ายมาก
<br>จบเรื่อง App และหยุดที่ AppSheet 

# Control Scope จบที่ Google Scope
<br>[ทำไมต้องทำเวปไซต์ ในเมื่อเก็บข้อมูลไว้ใน git ก็ได้](https://sites.google.com/d/1A3K4FHhZAQEJCpYwcgMcLGLmRZvjvstj/p/1b7GcI7itfqae6NcDEmeyehowZ883MKv0/edit)
<br>Google Scripts มีตัวอย่างการเขียนเริ่มที่ Google sheet [การเขียนสคริปสั่งงาน Google](https://script.gs/)

# รวมคำสั่งใน CMD ดูแล้วประโยชน์น้อย
<br> คำสั่งดูว่าคอมเคยต่อ wifi อะไรบ้าง netsh wlan show profile
<br> คำสั่งดูรหัสที่เคยต่อ wifi ให้เซฟลงไดรฟ c netsh wlan export profile folder=c:\ key=clear
<br>https://tips.thaiware.com/578.html 
<br>https://www.varietypc.net/th/200-run-command-prompt/

# Colab ดูมีอนาคตน่าฝึกฝนต่อมีของเล่นเยอะ
การเช็คความเร็วเน๊ตได้ง่ายด้วย คำสั่ง speedtest-cli 
!pip install speedtest-cli (ถ้าต้องการ import library python ที่ไม่มีอยู่ใน Colab ให้ใช้ !pip install)
!speedtest-cli (bit/s)
https://github.com/sivel/speedtest-cli/wiki
<br> **machine learning ใน Colab น่าศึกษา**
คำสั่งที่สำคัญๆ keras ,import TensorFlow ,from TensorFlow.keras import learning
<br> [ลองฝึกในลิงค์นี้ดู](https://colab.research.google.com/drive/1cAODZJQPmwvKN8gY8OTclj-DCD10ojcC)
<br> ตัวอย่างโปรแกรมเต่าใน Colab
!pip3 install ColabTurtle
from ColabTurtle.Turtle import *
https://github.com/tolgaatam/ColabTurtle รวมคำสั่ง Turtle ไว้ใช้
penup() ไว้ครอบ goto(x,y)pendown() จะได้ไม่เห็นเส้น

# เขียนเวปด้วย django ยังเห็นประโยชน์น้อยสำรับตัวเองอยู่ ข้ามไปก่อน
www.pypi.org เป็นที่รวบรวม package python ที่เราต้องการใช้ฟรี django ก็เป็นหนึ่งใน package python 
การเรียกใช้ง่ายๆ pip install package ที่ต้องการเลย
pip install virsualenv
การสร้างสิ่งแวดล้อมใน cmd
คำสั่ง virtualenv TADA_ENV
ใช้คำสั่ง activate โดย .\TADA_ENV\Scripts\activate
เราก็จะได้สิ่งแวดล้อม <TADA_ENV> แต่มาคิดดูใช้ colab ก็ได้เหมือนกันนะ ขอบเขตสิ่งแวดล้อมก็คือไฟล์ lab 
ความสำคัญคือเราสามารถเรียกใช้คำสั่ง python ใหม่ๆ ได้ภายในสิ่งแวดล้อมที่สร้างใหม่นี้ เช่นเรียก package dejango ในสิ่งแวดล้อมใหม่นี้
สร้างหน้าเวปไซต์ได้ง่ายด้วยคำสั่ง django-admin startproject tadawebsite
เข้าไปในโฟลเดอร์ tadawebsite จะมี manage.py คือคำสั่งของ django ในการรันโปรแกรม เช่น runserver ,createsuperuser ,makemigrations, migrate
localhost:8000 เข้าหน้าเวปเลย

# อันนี้เจ๋งมากเลย Google Assistant ใน Notebook ใช้ JavaScripts เขียน
[ลิงค์ไป code](https://github.com/Melvin-Abraham/Google-Assistant-Unofficial-Desktop-Client/wiki/Setup-Authentication-for-Google-Assistant-Unofficial-Desktop-Client)
<br>[ควบคุมเม้าส์ด้วย PyUserInput](https://github.com/SavinaRoja/PyUserInput)
<br>มีการอัพเดทใน github 
# Jina คือ Neural Search [Jina](https://docs.jina.ai/get-started/neural-search/) 
เรียนรู้วิธีใช้ jina

https://imageai.readthedocs.io/en/latest/detection/

# AI กล้องวงจรปิด 
- เป็นโปรเจ็คที่น่าทำมาก [Project กล้องวงจรปิด](https://www.patanasongsivilai.com/blog/object-dection/)
- เริ่มโปรเจ็คใหม่ AI กล้องวงจรปิด ด้วย [Google Colab](https://colab.research.google.com/drive/1TMs84bl_pqi-VfpTZXkmQvgseqA3ueSq#scrollTo=X5z0laDHtgO7)
- Google [สอนทำ face detection ดีมาก](https://teachablemachine.withgoogle.com/)
